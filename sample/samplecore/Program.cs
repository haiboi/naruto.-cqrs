using Naruto.CQRS;
using Naruto.CQRS.Command;
using Naruto.CQRS.Query;
using samplecore;

var builder = WebApplication.CreateBuilder(args);
builder.Services.AddNarutoCqrs(typeof(Program));
var app = builder.Build();

app.MapGet("/", () => "Hello World!");
app.MapGet("/command", async (ICommandDispatcher commandDispatcher) =>
{
    await commandDispatcher.SendAsync(new TestCommandEvent("zhangsan")
    {
    });
});
app.MapGet("/query", async (IQueryDispatcher commandDispatcher, ILogger<Program> _logger) =>
{
    var res = await commandDispatcher.SendAsync<TestQueryEvent, string>(new TestQueryEvent()
    {
        name = "zhangsan"
    });
    _logger.LogInformation("return {Res}", res);
});
app.Run();