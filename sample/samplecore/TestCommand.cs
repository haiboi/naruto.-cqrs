using Naruto.CQRS.Command;

namespace samplecore;

public class TestCommandEvent : ICommandEvent
{
    public TestCommandEvent(string name)
    {
        this.name = name;
    }

    public string name { get; set; }
}

public class TestCommand : ICommand<TestCommandEvent>
{
    public async Task HandleAsync(TestCommandEvent eEvent, CancellationToken cancellationToken = default)
    {
        Console.WriteLine("command");
        await Task.Delay(1 * 1000);
    }
}