using Naruto.CQRS.Query;

namespace samplecore;

public class TestQueryEvent : IQueryEvent
{
    public string name { get; set; }
}

public class TestQuery : IQuery<TestQueryEvent, string>
{
    private readonly ILogger<TestQuery> _logger;

    public TestQuery(ILogger<TestQuery> logger)
    {
        _logger = logger;
    }

    public Task<string> HandleAsync(TestQueryEvent @event, CancellationToken cancellationToken = default)
    {
        _logger.LogInformation("testQuery");
        return Task.FromResult(@event.name);
    }
}