namespace Naruto.CQRS.Command;

/// <summary>
/// 
/// </summary>
public interface ICommand
{
}

/// <summary>
/// 命令接口
/// </summary>
/// <typeparam name="TEvent"></typeparam>
public interface ICommand<in TEvent> : ICommand where TEvent : ICommandEvent
{
    /// <summary>
    /// 事件处理
    /// </summary>
    /// <param name="eEvent"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    Task HandleAsync(TEvent eEvent, CancellationToken cancellationToken = default);
}