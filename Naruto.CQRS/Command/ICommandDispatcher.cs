namespace Naruto.CQRS.Command;

public interface ICommandDispatcher
{
    /// <summary>
    /// 发送
    /// </summary>
    /// <param name="event"></param>
    /// <param name="cancellationToken"></param>
    /// <typeparam name="TEvent"></typeparam>
    /// <returns></returns>
    Task SendAsync(ICommandEvent @event, CancellationToken cancellationToken = default);
}