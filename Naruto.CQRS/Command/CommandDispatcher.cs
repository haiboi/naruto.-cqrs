using System.Dynamic;
using System.Linq.Expressions;
using Microsoft.Extensions.DependencyInjection;

namespace Naruto.CQRS.Command;

/// <summary>
/// 内部实现
/// </summary>
public sealed class CommandDispatcher : ICommandDispatcher
{
    /// <summary>
    /// 
    /// </summary>
    private readonly IServiceProvider _serviceProvider;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="serviceProvider"></param>
    public CommandDispatcher(IServiceProvider serviceProvider)
    {
        _serviceProvider = serviceProvider;
    }

    /// <summary>
    /// 发送消息
    /// </summary>
    /// <param name="event"></param>
    /// <param name="cancellationToken"></param>
    public async Task SendAsync(ICommandEvent @event, CancellationToken cancellationToken = default)
    {
        //获取服务信息
        var service = _serviceProvider.GetRequiredService(typeof(ICommand<>).MakeGenericType(@event.GetType()));
        var p1 = Expression.Parameter(service.GetType());
        var call = Expression.Call(p1, nameof(ICommand<ICommandEvent>.HandleAsync), null, Expression.Constant(@event), Expression.Constant(cancellationToken));
        //执行方法
        await ((Expression.Lambda(call, p1).Compile().DynamicInvoke(service) as Task)!);
    }
}