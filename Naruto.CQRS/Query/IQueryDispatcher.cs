namespace Naruto.CQRS.Query;

public interface IQueryDispatcher
{
    /// <summary>
    /// 发送消息
    /// </summary>
    /// <param name="event"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    Task<TResult> SendAsync<TEvent,TResult>(TEvent @event, CancellationToken cancellationToken = default)where TEvent : IQueryEvent;
}