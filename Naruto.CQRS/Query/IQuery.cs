namespace Naruto.CQRS.Query;

/// <summary>
/// 
/// </summary>
public interface IQuery
{
}

public interface IQuery<in TEvent, TResult> : IQuery where TEvent : IQueryEvent
{
    /// <summary>
    /// 消息处理
    /// </summary>
    /// <param name="event"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    Task<TResult> HandleAsync(TEvent @event, CancellationToken cancellationToken = default);
}